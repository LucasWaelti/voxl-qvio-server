#!/bin/bash


## special toolchain in this project for 32-bit qrb5165 builds
TOOLCHAIN_QRB5165_32="arm-gnueabi-7.toolchain.cmake"

# placeholder in case more cmake opts need to be added later
EXTRA_OPTS=""

## this list is just for tab-completion
AVAILABLE_PLATFORMS="qrb5165 apq8096 native"

print_usage(){
	echo ""
	echo " Build the current project in one of these modes based on build environment."
	echo ""
	echo " Usage:"
	echo ""
	echo "  ./build.sh apq8096"
	echo "        Build 32-bit binaries for apq8096 native in voxl-emulator"
	echo ""
	echo "  ./build.sh qrb5165"
	echo "        Build 32-bit binaries for qrb5165 with gcc7 in qrb5165-emulator"
	echo ""
	echo "  ./build.sh native"
	echo "        Build with the native gcc/g++ compilers."
	echo ""
	echo ""
}


case "$1" in
	apq8096)
		mkdir -p build
		cd build
		cmake -DPLATFORM=APQ8096 ${EXTRA_OPTS} ../
		make -j$(nproc)
		cd ../
		;;
	qrb5165)
		mkdir -p build
		cd build
		cmake -DCMAKE_TOOLCHAIN_FILE=${TOOLCHAIN_QRB5165_32} -DPLATFORM=QRB5165 ${EXTRA_OPTS} ../
		make -j$(nproc)
		cd ../
		;;

	native)
		mkdir -p build
		cd build
		cmake ${EXTRA_OPTS} ../
		make -j$(nproc)
		cd ../
		;;

	*)
		print_usage
		exit 1
		;;
esac



